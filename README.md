# Scaleego server api

# Commit

## Format 
> type($scope): $message
> 
> $body
## Type

Must be one of the following:

 -   **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
 -   **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
 -   **docs**: Documentation only changes
 -   **feat**: A new feature
 -   **fix**: A bug fix
 -   **perf**: A code change that improves performance
 -   **refactor**: A code change that neither fixes a bug nor adds a feature
 -   **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
 -   **test**: Adding missing tests or correcting existing tests
## Example
> fix(release): need to depend on latest rxjs and zone.js
>
>The version in our package.json gets copied to the one we publish, and users need the latest of these.


# Config
Find all env variable in **src/config/.env**

Use **src/config/.env.local** file to overwrite variables

 - **PORT**=8000
 - **MONGO_DATABASE_URI**=mongodb+srv://user:pass@url.net/db?
 - **MONGO_DATABASE_NAME**=database  
 - **MARIA_DATABASE_URI**=localhost  
 - **MARIA_DATABASE_PORT**=3306   
 - **MARIA_DATABASE_NAME**=database  
 - **MARIA_DATABASE_USER**=root  
 - **MARIA_DATABASE_PASSWORD**=password  
 - **MARIA_DATABASE_TIMEZONE**=Etc/GMT0
