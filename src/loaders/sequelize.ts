import {Sequelize} from "sequelize";
import config from "../config";
import {logger} from "./winston";

export default async (): Promise<any> => {
    return  sequelize.authenticate();
}

export const sequelize = new Sequelize(config.database.maria.name, config.database.maria.user, config.database.maria.password, {
    host: config.database.maria.uri,
    port: Number(config.database.maria.port),
    dialect: 'mariadb',
    logging: msg => logger.verbose(msg),
    dialectOptions: {
        timezone: config.database.maria.timezone
    },

})
