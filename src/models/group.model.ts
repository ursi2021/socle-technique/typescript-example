import {Sequelize, DataTypes, Model, ModelCtor, Optional} from 'sequelize';
import Loader from "../loaders";
import {sequelize} from "../loaders/sequelize";
import {logger} from "../loaders/winston";


export class Group extends Model {
    public id!: number;
    public name!: string;
    public coverage!: number;
}

Group.init({
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: new DataTypes.STRING(128),
            allowNull: false,
        },
        coverage: {
            type: new DataTypes.FLOAT(),
            allowNull: false,
            defaultValue: 0,
        },
    }, {
        tableName: "groups",
        sequelize // this bit is important
    }
)

Group.sync({force: false}).then(() => logger.info('Group table created'));
