import dotenv from 'dotenv-flow';
// config() will read your .env file, parse the contents, assign it to process.env.
dotenv.config({path:'src/config/'});

export default {
    port: process.env.PORT,
    database: {
        mongo: {
            uri: process.env.MONGO_DATABASE_URI,
            name: process.env.MONGO_DATABASE_NAME
        },
        maria: {
            uri: process.env.MARIA_DATABASE_URI,
            port: process.env.MARIA_DATABASE_PORT,
            name: process.env.MARIA_DATABASE_NAME,
            user: process.env.MARIA_DATABASE_USER,
            password: process.env.MARIA_DATABASE_PASSWORD,
            timezone: process.env.MARIA_DATABASE_TIMEZONE
        },
        mailchimp: {
            apiKey: process.env.MAILCHIMP_API_KEY,
            sender: process.env.MAILCHIMP_SENDER,
        }
    }
}